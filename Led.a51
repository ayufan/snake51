#include "Main.inc"

LED_SEG  SEGMENT  CODE
RSEG  LED_SEG

PUBLIC initLed, clearLed, setLedDigit, setLedEncoded, showNextLedEncodedColumn

;--------------------------------------------------------------------------------------------------------

initLed:
		mov		P5MDOUT, #0FFh
		clr		LCD_LED
		mov		P6MDOUT, #0FFh
		mov		P1MDIN, #0F8h
		mov		P1MDOUT, #08h
		ret

;--------------------------------------------------------------------------------------------------------

setLedDigit:
		; Calculate Offset
		mov		B, #35
		mul		AB

		; Calculate Low Address
		add		A, #Low(LedDigitsTable)
		mov		DPL, A

		; Calculate High Address
		mov		A, #High(LedDigitsTable)
		addc	A, B
		mov		DPH, A
		jmp		setLedEncoded

clearLed:
		mov		DPTR, #LedClearTable

setLedEncoded:
		mov		led_haddr, DPH
		mov		led_laddr, DPL
		ret

;--------------------------------------------------------------------------------------------------------
; A - column
; DPTR - addr

sendLedEncodedColumn:
		; 6 insignificant bits
		push	ACC
		mov		R0, #6
		call	sendLedData
		pop		ACC

		; 5 column index bits
		mov		R0, #5
		call	sendLedData
		
		; 7 columns each 3 bits
		mov		R2, #7
	sendLedEncodedColumn_loop:
		; Fetch next column
		mov		A, #0
		movc	A, @A+DPTR
		inc		DPTR
		clr		C
		subb	A, #'0'
		cpl		A

		; Send data
		mov		R0, #3
		mov		A,#0FBh
		call	sendLedData
		djnz	R2, sendLedEncodedColumn_loop
		
		; Save registers state
		setb	RGB_LATCH
		nop
		nop
		clr		RGB_LATCH
		ret

;--------------------------------------------------------------------------------------------------------

showNextLedEncodedColumn:
		push	DPL
		push	DPH
		push	ACC
		push	B

		; Get address of segment
	 	mov		A, timerCounter
		mov		B, #5
		div		AB
		mov		A, #4
		clr		C
		subb	A, B
		mov		B, #7
		mul		AB

		; Calculate low addr
		add		A, led_laddr
		mov		DPL, A

		; Calculate high addr
		mov		A, led_haddr
		addc	A, B
		mov		DPH, A

		; Get number of column
		mov		A, P2
		cpl		A

		; Send led encoded column
		call	sendLedEncodedColumn
	
		pop		ACC
		pop		B
		pop		DPH
		pop		DPL
		ret

;--------------------------------------------------------------------------------------------------------
; A - buffer
; R0 - bits

sendLedData:
		clr		C
	sendLedData_loop:
		rrc		A
		mov		RGB_DATA, C
		setb	RGB_CLK
		nop
		nop
		clr		RGB_CLK
		djnz	R0, sendLedData_loop
		ret

;--------------------------------------------------------------------------------------------------------

LedClearTable:
		db		'0000000'
		db		'0000000'
		db		'0000000'
		db		'0000000'
		db		'0000000'

LedDigitsTable:		  // 7 * 5 = 35
		; 0
		db		'1222221'
		db		'2111112'
		db		'2111112'
		db		'2111112'
		db		'1222221'

		; 1
		db 		'1121112'
		db		'1211112'
		db		'2222222'
		db		'1111112'
		db		'1111112'

		; 2
		db		'1211112'
		db		'2111122'
		db		'2111212'
		db		'2112112'
		db		'1221112'

		; 3
		db		'2444424'
		db		'2444442'
		db		'2424442'
		db		'2242442'
		db		'2444224'

		; 4
		db		'1112211'
		db		'1121211'
		db		'1211211'
		db		'2222222'
		db		'1111211'
		
		; 5
		db		'2221121'
		db		'2121112'
		db		'2121112'
		db		'2121112'
		db		'2112221'

		; 6
		db		'4422224'
		db		'4242442'
		db		'2442442'
		db		'2442442'
		db		'4444224'

		; 7
		db		'2444444'
		db		'2444444'
		db		'2442222'
		db		'2424444'
		db		'2244444'

		; 8
		db		'1221221'
		db		'2112112'
		db		'2112112'
		db		'2112112'
		db		'1221221'

		; 9
		db		'1221111'
		db		'2112112'
		db		'2112112'
		db		'2112112'
		db		'1221221'

END
