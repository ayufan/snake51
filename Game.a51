#include "Main.inc"

GAME_SEG  SEGMENT  CODE
RSEG  GAME_SEG

EXTRN CODE (initKeyboard, clearKeyboard, readKeyboard, waitForAnyKey)
EXTRN CODE (initDigits, setDigitsInt, showNextDigit, clearDigits)
EXTRN CODE (initTimer, timerWait, onTimer)
EXTRN CODE (initLcd, setXY, setCursor, clearText, clearGraph, clearGraphFade)
EXTRN CODE (print, setPixel, setPixel2, clearPixel, clearPixel2)
EXTRN CODE (initLed, clearLed, setLedDigit, setLedEncoded)

PUBLIC game

;------------------------------------------------------------------------------------------------------

game:
		; Clear graphics & text
		call	clearDigits
		call	clearText
		call	clearGraph
		call	clearLed

		mov		A, #0
		call	setLedDigit

	main_startNewGame:
		call	showGameInfo
		call	waitForAnyKey
		call	clearText

		; Zero game score
		mov		A, #0
		call	setDigitsInt

		; Draw horizontal border
		mov		R0, #VWIDTH
	main_drawBorderHorizontal:
		mov		A, R0
		dec		A
		mov		B, #0
		call	setPixel2
		mov		A, R0
		dec		A
		mov		B, #VHEIGHT-1
		call	setPixel2
		mov		A,#1
		call	sleep
		djnz	R0,main_drawBorderHorizontal

		; Draw vertical border
		mov		R0, #VHEIGHT-2
	main_drawBorderVertical:
		mov		A, #0
		mov		B, R0
		call	setPixel2
		mov		A, #VWIDTH-1
		mov		B, R0
		call	setPixel2
		mov		A,#2
		call	sleep
		djnz	R0,main_drawBorderVertical
				
		; Add point to list & turn on pixel
		mov		lengthOfSnake, #1

		mov		DPTR,#snakeY
		mov		A,#VHEIGHT/2
		movx	@DPTR, A
		mov		B, A

		mov		DPTR,#snakeX
		mov		A,#VWIDTH/2
		movx	@DPTR, A
		
		; Set pixel
		call	setPixel2

		; Zero all data
		mov		numberOfTriggers, #0
		mov		gameScore, #0
		mov		expandTime, #0
		clr		snakeDirUpDown
		clr		snakeDirLeftRight

		; New trigger
		call	newTrigger


	;------------------------------------------------------------------------------------------------------
	; MAIN LOOP

	main_loop:

	;------------------------------------------------------------------------------------------------------
	; Move snake in direction (on return R4, R5 holds new x, y)

		; Check up-key
		mov		A, changedKeys
		jb		ACC.5, main_downButtonPressed
		mov		A, changedKeys+1
		jb		ACC.0, main_leftButtonPressed	
		jb		ACC.2, main_rightButtonPressed
		jb		ACC.5, main_upButtonPressed	
		jmp		main_moveSnake

	main_upButtonPressed:
		jb		snakeDirUpDown, main_moveSnake
		setb	snakeDirUpDown
		clr		snakeDirLeftRight
		jmp		main_moveSnake	

	main_downButtonPressed:
		jb		snakeDirUpDown, main_moveSnake
		setb	snakeDirUpDown
		setb	snakeDirLeftRight
		jmp		main_moveSnake

	main_leftButtonPressed:
		jnb		snakeDirUpDown, main_moveSnake
		clr		snakeDirUpDown
		clr		snakeDirLeftRight
		jmp		main_moveSnake

	main_rightButtonPressed:
		jnb		snakeDirUpDown, main_moveSnake
		clr		snakeDirUpDown
		setb	snakeDirLeftRight
		jmp		main_moveSnake


	; Move snake in direction
	main_moveSnake:
		; R5 - current y
		mov		DPTR, #snakeY
		movx	A, @DPTR
		mov		R5, A

		; R4 - current x
		mov		DPTR, #snakeX
		movx	A, @DPTR
		mov		R4, A

		jb		snakeDirUpDown, main_moveSnakeUpOrDown

	; Try to move left or right
	main_moveSnakeLeftOrRight:
		; Check selected direction
		jb		snakeDirLeftRight, main_moveSnakeRight

		; Try to move left
		main_moveSnakeLeft:
			; Check left bound (> 0)
			cjne	R4, #1, main_moveSnakeLeftChecked
			jmp		main_showGameOver

		main_moveSnakeLeftChecked:
			dec		R4	; x--
			jmp		main_snakeMoved


		; Try to move right
		main_moveSnakeRight:
			; Check right bound (< VWIDTH-1)
			cjne	R4, #VWIDTH-2, main_moveSnakeRightChecked
			jmp		main_showGameOver

		main_moveSnakeRightChecked:
			inc		R4 ; x++
			mov		A, #1
			jmp		main_snakeMoved

	; Try to move up or down
	main_moveSnakeUpOrDown:
		; A - current y
		mov		DPTR, #snakeY
		movx	A, @DPTR

		; Check selected direction
		jb		snakeDirLeftRight, main_moveSnakeDown

		; Try to move up
		main_moveSnakeUp:
			; Check up bound (> 0)
			cjne	R5, #1, main_moveSnakeUpChecked
			jmp		main_showGameOver

		main_moveSnakeUpChecked:
			dec		R5 	; y--
			jmp		main_snakeMoved

		; Try to move down
		main_moveSnakeDown:
			; Check down bound (< VHEIGHT-1)
			cjne	R5, #VHEIGHT-2, main_moveSnakeDownChecked
			jmp		main_showGameOver

		main_moveSnakeDownChecked:
			inc		R5 	; y++
			jmp		main_snakeMoved

	main_snakeMoved:
	  	; Check if we hit your back
		call	checkIfCollides
		jnz		main_showGameOver

	;------------------------------------------------------------------------------------------------------
	; add point to list

		; Create one new point
		call	moveSnake

		; Save new y
		mov		DPTR, #snakeY
		mov		A, R5
		movx	@DPTR, A
		mov		B, A

		; Save new x
		mov		DPTR, #snakeX
		mov		A, R4
		movx	@DPTR, A

		; Mark new pixel
		call	setPixel2

	;------------------------------------------------------------------------------------------------------
	; check if we hit trigger (if not clear last pixel, if yes increase snake lengthOfSnake)

		; Compare x == triggerX
		mov		A, triggerX
		clr		C
		subb	A, R4
		jnz		main_expandTime

		; Compare y == triggerY
		mov		A, triggerY
		clr		C
		subb	A, R5
		jnz		main_expandTime


		; We hit trigger!	 
		; gameScore++
		inc		gameScore	 
		
		; Save new gameScore
	main_updateScore:
		mov		A, gameScore
		call	setDigitsInt

		; Create new trigger
		call	newTrigger

	main_expandTime:
		mov		A, expandTime
		jz		main_clearLastPoint
		dec		expandTime

		; Try to increse lengthOfSnake
		inc		lengthOfSnake
		mov		A, lengthOfSnake
		jnz		main_wait
		mov		lengthOfSnake,#MAX_LENGTH
		
		; Clear last pixel (it's quite fast)		  
	main_clearLastPoint:		
		; Get y after last one
		mov		DPTR, #snakeY
		mov		DPL, lengthOfSnake
		inc		DPTR
		movx	A, @DPTR
		mov		B, A
		
		; Get x after last one
		mov		DPTR, #snakeX
		mov		DPL, lengthOfSnake
		inc		DPTR
		movx	A, @DPTR
		
		; Clear pixel								
		call	clearPixel2

	;------------------------------------------------------------------------------------------------------
	; wait

		; Wait for next interrupt
	main_wait:
		call	timerWait						  
		jmp		main_loop 

	;------------------------------------------------------------------------------------------------------
	; show game over
			   
		; Show gameover
	main_showGameOver:
		; Wait long triggerLeftTime
		mov		A, #100 ; 1s
		call	sleep

		; Clear screen
		call	clearGraphFade
		call	clearText

		; Show fancy text :)
		mov		A, #6
		mov		B, #4
		call	setXY
		mov		DPTR, #GameOverText
		call	print

		; Restart game
		jmp		main_startNewGame

;------------------------------------------------------------------------------------------------------

showGameInfo:
		; Show text
		mov		A, #7
		mov		B, #1
		call	setXY
		mov		DPTR, #GameNameText
		call	print
		
		mov		A, #1
		mov		B, #2
		call	setXY
		mov		DPTR, #GameAuthorText
		call	print
		
		mov		A, #3
		mov		B, #6
		call	setXY
		mov		DPTR, #PressAnyKeyText
		call	print
		ret
	
;------------------------------------------------------------------------------------------------------

newTrigger:
		inc		numberOfTriggers
		mov		expandTime, #EXPAND_TIME

	newTrigger_newPoint:
		; Rand y = rand() % VHEIGHT
		call	rand8
		mov		B, #VHEIGHT-2
		div		AB
		inc		B
		mov		R5, B
		
		; Rand x = rand() % VWIDTH
		call	rand8
		mov		B, #VWIDTH-2
		div		AB
		inc		B
		mov		R4, B

		; Check if point collides
		call	checkIfCollides
		cjne	A, #0, newTrigger_newPoint

		; Save point
		mov		triggerX, R4
		mov		triggerY, R5

		; Mark new trigger
		mov		A, R4
		mov		B, R5
		call	setPixel2
		ret

;------------------------------------------------------------------------------------------------------
; R4 - x
; R5 - y
; result: A 1 - true, 0 - false

checkIfCollides:
		; Initialize loop (we assume that lengthOfSnake > 0)
		mov		R0, lengthOfSnake
		mov		DPL, #0

	checkIfCollides_loop:
		; Compare x == *p
		mov		DPH, #High(snakeX)
		movx	A, @DPTR
		clr		C		
		subb	A, R4
		jnz		checkIfCollides_next

		; Compare y == *++p
		mov		DPH, #High(snakeY)
		movx	A, @DPTR		
		clr		C
		subb	A, R5
		jnz		checkIfCollides_next

		; We hit your back!
		mov		A, #1
		ret 

	 checkIfCollides_next:
		inc		DPL
		djnz	R0, checkIfCollides_loop
		mov		A, #0
		ret

;------------------------------------------------------------------------------------------------------

GameNameText:
	db 'SNAKE51', 0
	
GameAuthorText:
	db 'ayufan(at)o2(dot)pl', 0

GameOverText:
	db 'GAME OVER', 0
	
PressAnyKeyText:
	db '(press any key)', 0

;------------------------------------------------------------------------------------------------------

moveSnake:
		; Move x
		mov		DPTR, #snakeX
		mov		R0, lengthOfSnake
		inc		R0
		movx	A, @DPTR
		mov		R1, A

	moveSnake_loopX:
		inc		DPTR
		movx	A, @DPTR
		xch		A, R1
		movx	@DPTR, A
		djnz	R0, moveSnake_loopX

		; Move y
		mov		DPTR, #snakeY
		mov		R0, lengthOfSnake
		inc		R0
		movx	A, @DPTR
		mov		R1, A

	moveSnake_loopY:
		inc		DPTR
		movx	A, @DPTR
		xch		A, R1
		movx	@DPTR, A
		djnz	R0, moveSnake_loopY
		ret

;------------------------------------------------------------------------------------------------------

sleep:
		mov R4, A
	sleep_loop2:
		mov	R5, #174
	sleep_loop:
		mov	R6, #56  
		djnz R6, $
		djnz R5, sleep_loop
		djnz R4, sleep_loop2
		ret

;------------------------------------------------------------------------------------------------------

rand8:	
		mov	A, rand8seed
		jnz	rand8b
		cpl	A
		mov	rand8seed, A
	rand8b:	
		anl	A, #10111000b
		mov	C, P
		mov	A, rand8seed
		rlc	A
		mov	rand8seed, A
		ret

;--------------------------------------------------------------------------------------------------------

END
