$NOMOD51
#include <C8051F060.inc>

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;	CONSTS
;
; syntax:
; lower-case - internal memory
; upper-case - consts
;


; MEMORY
gameScore			equ	5Ch	; [1] -> 5Bh (current gameScore)
lengthOfSnake		equ	5Eh	; [1] -> 5Dh (number of points)
numberOfTriggers	equ 5Fh ; [1] -> 5Fh (number of numberOfTriggers)
expandTime			equ	68h

snakeX				equ	100h
snakeY				equ 200h

triggerX			equ	5Ah	; [1]
triggerY			equ 5Bh ; [1]

snakeDirUpDown		bit	2	 	; 00 - up, 01 - down
snakeDirLeftRight	bit 3		; 10 - left, 11 - right

; CONSTS
WIDTH				equ	128
HEIGHT				equ	64
VWIDTH				equ	64
VHEIGHT				equ	32
MAX_LENGTH			equ	0FFh
START_LENGTH		equ	20
MAX_TIME			equ	10
EXPAND_TIME			equ	10

; DIGITS
DIGIT0				equ	P2.0
DIGIT1				equ	P2.1
DIGIT2				equ	P2.2
DIGIT3				equ	P2.3
DIGIT4				equ	P2.4
DIGITSAVE			equ	P5.6
digits				equ 55h	; [5] -> 55h-59h

; LCD HANDLDING CONSTS
LCD_CE				equ	P5.1
LCD_RW				equ	P5.0
LCD_CD				equ	P5.2
LDC_BL				equ	P5.3

LCD_G_BASE			equ	0H ; has to be xx00h	
LCD_T_BASE			equ	1700H ; has to be xx00h	
LCD_BYTES_PER_ROW	equ	22

; LCD LED HANDLING CONSTS
LCD_LED				equ	P5.3
RGB_DATA			equ	P6.4
RGB_CLK				equ	P6.5
RGB_LATCH			equ	P1.3
led_laddr			equ	07Dh
led_haddr			equ	07Eh

; KEYBOARD
changedKeys			equ	51h	; [2] -> 51h-52h
currentKeys			equ	53h	; [2] -> 53h-54h

; TIMER
TOS					equ	30h	
ready				bit	0
tick				bit 1
T0_TMOD				equ	1h
T0_COUNTER			equ	65536 - 500
T1_COUNTER			equ	65536 - 500 * 60
timerCounter		equ	50h	 ; [1] -> 50h
TIMER_COUNTER_MAX	equ	50

; BUZZER
BUZZER				equ P5.7

; UTILS
rand8seed			equ	7Fh
