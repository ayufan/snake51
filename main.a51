#include "Main.inc"

EXTRN CODE (initKeyboard, initDigits, initTimer, initLcd, initLed, onTimer, onTick, game)

;------------------------------------------------------------------------------------------------------
		
		cseg 	AT 0000h
		jmp		start
	
		org		000BH
		jmp		onTimer

		org		001BH
		jmp		onTick

start:
		mov   	WDTCN, #0DEh
		mov   	WDTCN, #0ADh

		mov  	SFRPAGE, #CONFIG_PAGE
		mov   	XBR2, #040h

		mov		SP, #TOS

		;call	initLed
		call	initLcd
		call	initKeyboard
		call	initDigits
		call	initTimer
		setb	EA
		jmp		game

END
