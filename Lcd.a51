#include "Main.inc"

LCD_SEG  SEGMENT  CODE
RSEG  LCD_SEG

PUBLIC initLcd, setXY, setCursor, clearText, clearGraph, clearGraphFade
PUBLIC print, setPixel, setPixel2, clearPixel, clearPixel2

;------------------------------------------------------------------------------------------------------

initLcd:
		; Set text base
		mov		A, #Low(LCD_T_BASE)
		call	dataToLcd
		mov		A, #High(LCD_T_BASE)
		call	dataToLcd
		mov		A, #40h
		call	cmdToLcd

		; Set text area
		mov		A, #LCD_BYTES_PER_ROW
		call	dataToLcd
		mov		A, #0
		call	dataToLcd
		mov		A, #41h
		call	cmdToLcd

		; Set graphics base
		mov		A, #Low(LCD_G_BASE)
		call	dataToLcd
		mov		A, #High(LCD_G_BASE)
		call	dataToLcd
		mov		A, #42h
		call	cmdToLcd

		; Set graphics area
		mov		A, #LCD_BYTES_PER_ROW
		call	dataToLcd
		mov		A, #0
		call	dataToLcd
		mov		A, #43h
		call	cmdToLcd

		; Set internal CGROM mode, OR mode
		mov		A, #80h ; 80h
		call	cmdToLcd

		; Cursor is 8 lines high
		;mov		A, #94h ; 0A7h
		;call	cmdToLcd

		; Put cursor at (x,y)
		mov		A, #0
		call	dataToLcd
		mov		A, #0
		call	dataToLcd
		mov		A, #21h
		call	cmdToLcd

		; Set display mode
		mov		A, #9Dh ; 9Dh
		call	cmdToLcd

		; Enable backlight
		mov		P5MDOUT, #08h
		setb	LDC_BL
		ret

;------------------------------------------------------------------------------------------------------
; A - x
; B - y

setXY:
		; Calculate address
		mov		R7, A
		mov		A, #LCD_BYTES_PER_ROW
		mul		AB
		add 	A, R7
		jnc 	setXY_nc
		inc 	B
	setXY_nc:
		call	dataToLcd
		mov		A, B
		add		A, #High(LCD_T_BASE)
		call	dataToLcd
		mov		A, #24h
		call	cmdToLcd
		ret	

;------------------------------------------------------------------------------------------------------
; A - x
; B - y

setCursor:
		call	dataToLcd
		mov		B, A
		call	dataToLcd
		mov		A, #21h
		call	cmdToLcd
		ret

;------------------------------------------------------------------------------------------------------

clearText:
		; Set pointer to text base
		mov		A, #Low(LCD_T_BASE)
		call	dataToLcd
		mov		A, #High(LCD_T_BASE)
		call	dataToLcd
		mov		A, #24h
		call	cmdToLcd
		
		; Clear all 1280 (10 * 128) bytes
		mov		R7, #10
	clearText_loop2:
		mov		R6, #128
	clearText_loop:
		mov		A, #0
		call	dataToLcd
		mov		A, #0C0h
		call	cmdToLcd
		djnz	R6, clearText_loop	
		djnz	R7, clearText_loop2
		ret

;------------------------------------------------------------------------------------------------------

clearGraph:
		; Set pointer to graphics base
		mov		A, #Low(LCD_G_BASE)
		call	dataToLcd
		mov		A, #High(LCD_G_BASE)
		call	dataToLcd
		mov		A, #24h
		call	cmdToLcd
		
		; Clear all 5120 (40 * 128) bytes
		mov		R7, #40
	clearGraph_loop2:
		mov		R6, #128
	clearGraph_loop:
		mov		A, #0h
		call	dataToLcd
		mov		A, #0C0h
		call	cmdToLcd
		djnz	R6, clearGraph_loop	
		djnz	R7, clearGraph_loop2
		ret
		
;------------------------------------------------------------------------------------------------------		
		
clearGraphFade:
	; Phase 1
		mov		R4, #0
	clearGraphFade1_loop:
		mov		R5, #0
	clearGraphFade1_loop2:
		mov		A,R4
		mov		B,R5
		call	setPixel
		inc		R5
		cjne	R5,#HEIGHT,clearGraphFade1_loop2
		inc		R4
		cjne	R4,#WIDTH,clearGraphFade1_loop

	; Phase 2
		mov		R4, #0
	clearGraphFade2_loop:
		mov		R5, #0
	clearGraphFade2_loop2:
		mov		A,R5
		mov		B,R4
		call	clearPixel
		inc		R5
		cjne	R5,#WIDTH,clearGraphFade2_loop2
		inc		R4
		cjne	R4,#HEIGHT,clearGraphFade2_loop
		ret		

;------------------------------------------------------------------------------------------------------
; DPTR - null-terminated text

print:
		mov		A, #0
		movc	A, @A+DPTR
		cjne	A, #0, print_dataWrite
		ret
	print_dataWrite:
		subb	A, #20h
		call	dataToLcd
		mov		A, #0C0h
		call	cmdToLcd
		inc		DPTR
		jmp		print

;------------------------------------------------------------------------------------------------------
; A - x
; B - y

setPixel:
		; Calculate R7=x%6, R6=x/6, A=y
		mov		R6, B
		mov		B, #6
		div		AB
		mov		R7, B
		xch 	A, R6

		; Calculate address
		mov		B, #LCD_BYTES_PER_ROW
		mul		AB
		add 	A, R6
		jnc 	setPixel_nc
		inc 	B
	setPixel_nc:

		; Send Data Address to LCD
		call	dataToLcd ; A = y * LCD_BYTES_PER_ROW + x/6

		mov		A, B
		call	dataToLcd ; A = B + High(LCD_G_BASE)

		mov		A, #24h
		call	cmdToLcd

		; Send Set Bit (R7) to LCD
		mov		A,#5
		clr		C
		subb	A,R7
		orl		A,#0F8h	; A = 0xF8 | (5-x%6)
		call	cmdToLcd
		ret

;------------------------------------------------------------------------------------------------------
; sets double pixel
; A - x
; B - y

setPixel2:
		setb	RS0

		; Calculate R7=x%6, R6=x/6, A=y
		mov		R6, B
		clr		C
		rlc		A
		mov		B, #6
		div		AB
		mov		R7, B
		xch 	A, R6
		clr		C
		rlc		A

		; Calculate base address
		mov		B, #LCD_BYTES_PER_ROW
		mul		AB
		add 	A, R6
		jnc 	setPixel2_nc
		inc 	B
	setPixel2_nc:

		; Send Data Address to LCD
		mov		R5, A
		call	dataToLcd ; A = y * LCD_BYTES_PER_ROW + x/6

		mov		A, B
		mov		R6, A
		call	dataToLcd ; A = B + High(LCD_G_BASE)

		mov		A, #24h
		call	cmdToLcd

		; Mark first pixel
		mov		A,#5
		clr		C
		subb	A,R7
		orl		A,#0F8h	; A = 0xF8 | (5-x%6)
		call	cmdToLcd

		; Mark second pixel
		mov		A,#4
		clr		C
		subb	A,R7
		orl		A,#0F8h	; A = 0xF8 | (4-x%6)
		call	cmdToLcd

		; Calculate shifted address
		mov		A, #LCD_BYTES_PER_ROW
		add		A, R5
		jnc		setPixel2_nc2
		inc		R6
	setPixel2_nc2:

		; Send Shifted Data Address to LCD
		call	dataToLcd

		mov		A, R6
		call	dataToLcd

		mov		A, #24h
		call	cmdToLcd
					 
		; Mark third pixel
		mov		A,#5
		clr		C
		subb	A,R7
		orl		A,#0F8h	; A = 0xF8 | (5-x%6)
		call	cmdToLcd

		; Mark forth pixel
		mov		A,#4
		clr		C
		subb	A,R7
		orl		A,#0F8h	; A = 0xF8 | (4-x%6)
		call	cmdToLcd

		clr		RS0
		ret

;------------------------------------------------------------------------------------------------------
; A - x
; B - y

clearPixel:
		; Calculate R7=x%6, R6=x/6, A=y
		mov		R6, B
		mov		B, #6
		div		AB
		mov		R7, B
		xch 	A, R6

		; Calculate address
		mov		B, #LCD_BYTES_PER_ROW
		mul		AB
		add 	A, R6
		jnc 	clearPixel_nc
		inc 	B
	clearPixel_nc:
		; Send Data Address to LCD
		call	dataToLcd ; A = y * LCD_BYTES_PER_ROW + x/6

		mov		A, B
		call	dataToLcd ; A = B + High(LCD_G_BASE)

		mov		A, #24h
		call	cmdToLcd

		; Send Clear Bit (R7) to LCD
		mov		A,#5
		clr		C
		subb	A,R7
		orl		A,#0F0h	; A = 0xF0 | (5-x%6)
		call	cmdToLcd
		ret

;------------------------------------------------------------------------------------------------------
; clears double pixel
; A - x
; B - y

clearPixel2:
		setb	RS0

		; Calculate R7=x%6, R6=x/6, A=y
		mov		R6, B
		clr		C
		rlc		A
		mov		B, #6
		div		AB
		mov		R7, B
		xch 	A, R6
		clr		C
		rlc		A

		; Calculate base address
		mov		B, #LCD_BYTES_PER_ROW
		mul		AB
		add 	A, R6
		jnc 	clearPixel2_nc
		inc 	B
	clearPixel2_nc:

		; Send Data Address to LCD
		mov		R5, A
		call	dataToLcd ; A = y * LCD_BYTES_PER_ROW + x/6

		mov		A, B
		mov		R6, A
		call	dataToLcd ; A = B + High(LCD_G_BASE)

		mov		A, #24h
		call	cmdToLcd

		; Clear first pixel
		mov		A,#5
		clr		C
		subb	A,R7
		orl		A,#0F0h	; A = 0xF0 | (5-x%6)
		call	cmdToLcd

		; Clear second pixel
		mov		A,#4
		clr		C
		subb	A,R7
		orl		A,#0F0h	; A = 0xF0 | (4-x%6)
		call	cmdToLcd

		; Calculate shifted address
		mov		A, #LCD_BYTES_PER_ROW
		add		A, R5
		jnc		clearPixel2_nc2
		inc		R6
	clearPixel2_nc2:

		; Send Shifted Data Address to LCD
		call	dataToLcd

		mov		A, R6
		call	dataToLcd

		mov		A, #24h
		call	cmdToLcd
					 
		; Clear third pixel
		mov		A,#5
		clr		C
		subb	A,R7
		orl		A,#0F0h	; A = 0xF0 | (5-x%6)
		call	cmdToLcd

		; Clear forth pixel
		mov		A,#4
		clr		C
		subb	A,R7
		orl		A,#0F0h	; A = 0xF0 | (4-x%6)
		call	cmdToLcd

		clr		RS0
		ret

;------------------------------------------------------------------------------------------------------

isLcdBusy:
		push	ACC
		setb	LCD_CD
		setb	LCD_RW
		mov		P7,#0FFh
	isLcdBusy_check:
		mov		R1,#10h
		djnz	R1,$
		clr		LCD_CE
		mov		R1,#10h
		djnz	R1,$
		mov		A,P7
		setb	LCD_CE
		jnb		ACC.0,isLcdBusy_check
		jnb		ACC.1,isLcdBusy_check
		mov		R1,#10h
		djnz	R1,$
		pop		ACC
		ret

;------------------------------------------------------------------------------------------------------

cmdToLcd:
		clr		EA
		call	isLcdBusy
		nop
		nop
		clr		LCD_RW
		setb	LCD_CD
		mov		P7,A
		clr		LCD_CE
		mov		R1,#10h
		djnz	R1,$
		setb	LCD_CE
		setb	EA
		ret

;------------------------------------------------------------------------------------------------------

dataToLcd:
		clr		EA
		call	isLcdBusy
		nop
		nop
		clr		LCD_RW
		clr		LCD_CD
		mov		P7,A
		clr		LCD_CE
		mov		R1,#10h
		djnz	R1,$
		setb	LCD_CE
		setb	EA
		ret

END
