#include "Main.inc"

TIMER_SEG  SEGMENT  CODE
RSEG  TIMER_SEG

EXTRN CODE (readKeyboard, clearKeyboard, showNextDigit, showNextLedEncodedColumn)

PUBLIC initTimer
PUBLIC timerWait
PUBLIC onTimer
PUBLIC onTick

;--------------------------------------------------------------------------------------------------------

initTimer:
		mov			SFRPAGE, #TIMER01_PAGE

		; Reset timer state
		mov			timerCounter, #TIMER_COUNTER_MAX

		; Configure Timers
		mov			TMOD, #10001b

		; Start Timer0
		mov			TH0, #High(T0_COUNTER)
		mov			TL0, #Low(T0_COUNTER)

		; Start counting :)
		setb		TR0
		setb		TR1

		mov			SFRPAGE, #CONFIG_PAGE

		setb		ET0
		setb		ET1
		ret

;--------------------------------------------------------------------------------------------------------

reloadTimer:
		mov			TH0, #High(T0_COUNTER)
		mov			TL0, #Low(T0_COUNTER)
		ret

;--------------------------------------------------------------------------------------------------------

onTimer:
		mov			SFRPAGE, #TIMER01_PAGE
		call		reloadTimer
		mov			SFRPAGE, #CONFIG_PAGE
		djnz		timerCounter, onTimer_exit
		mov 		timerCounter, #TIMER_COUNTER_MAX
		setb		ready
	onTimer_exit:
		inc			rand8seed

		; Protected section
		setb		RS1
		call		showNextDigit
		;call		showNextLedEncodedColumn
		call		readKeyboard
		clr			RS1
		reti

;--------------------------------------------------------------------------------------------------------

onTick:
		mov			SFRPAGE, #TIMER01_PAGE
		mov			TH1, #High(T1_COUNTER)
		mov			TL1, #Low(T1_COUNTER)
		mov			SFRPAGE, #CONFIG_PAGE
		setb		tick
		reti

;--------------------------------------------------------------------------------------------------------

timerWait:
		call		clearKeyboard
		jnb			ready,$
		clr			ready
		ret

END
