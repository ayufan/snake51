#include "Main.inc"

KEYBOARD_SEG  SEGMENT  CODE
RSEG  KEYBOARD_SEG

EXTRN CODE (timerWait)

PUBLIC initKeyboard
PUBLIC clearKeyboard
PUBLIC readKeyboard
PUBLIC waitForAnyKey

; How it works
;
; There're 4 cases (1-up, 0-down)
; 
; 1) before=1	now=1		=>		changed=0	current=1
; 2) before=1	now=0		=>		changed=1	current=0
; 3) before=0	now=0		=>		changed=0	current=0
; 4) before=0	now=1		=>		changed=0	current=1
;
;	current=0
;	changed|=before & ~now

;------------------------------------------------------------------------------------------------------

initKeyboard:
		mov		currentKeys, #0FFh
		mov		currentKeys+1, #0FFh
		mov		changedKeys, #0h
		mov		changedKeys+1, #0h
		ret

;------------------------------------------------------------------------------------------------------

clearKeyboard:
		mov		changedKeys, #0h
		mov		changedKeys+1, #0h
		ret

;------------------------------------------------------------------------------------------------------

readKeyboard:
		push	ACC

		mov		A,#0FFh
		mov		P3,A

		; Read Column 0 state
		mov		A,#01111111b
		mov		P3,A
		mov		R1,#8h
		djnz	R1,$
		mov		A,P3
		anl		A,#0Fh
		mov		R7,A

		; Read Column 1 state
		mov		A,#10111111b
		mov		P3,A
		mov		R1,#8h
		djnz	R1,$
		mov		A,P3
		anl		A,#0Fh

		; Concencate results from Column 0 & 1
		swap	A				
		orl		A,R7

		; Get changed state
		push	ACC
		xch		A,currentKeys
		mov		R7,A
		pop		ACC
		cpl		A
		anl		A,R7
		orl		changedKeys,A
		
		

		; Read Column 2 state
		mov		A,#11011111b
		mov		P3,A
		mov		R1,#6h
		djnz	R1,$
		mov		A,P3
		anl		A,#0Fh
		mov		R7,A

		; Read Column 3 state
		mov		A,#11101111b
		mov		P3,A
		mov		R1,#6h
		djnz	R1,$
		mov		A,P3
		anl		A,#0Fh

		; Concencate results from Column 2 & 3
		swap	A			
		orl		A,R7

		; Get changed state
		push	ACC
		xch		A,currentKeys+1
		mov		R7,A
		pop		ACC
		cpl		A
		anl		A,R7
		orl		changedKeys+1,A

		pop		ACC
		ret
		
;------------------------------------------------------------------------------------------------------

waitForAnyKey:
		; Wait for next interrupt
		call	timerWait
		
		; Check if any key is pressed
		mov		A, changedKeys
		orl		A, changedKeys+1
		jz		waitForAnyKey
		ret

END
