#include "Main.inc"

DIGITS_SEG  SEGMENT  CODE
RSEG  DIGITS_SEG

PUBLIC initDigits
PUBLIC setDigitsInt
PUBLIC showNextDigit
PUBLIC clearDigits

;--------------------------------------------------------------------------------------------------------

initDigits:
		mov		P2, #0FEh
		ret

;--------------------------------------------------------------------------------------------------------

clearDigits:
		mov		digits, #0
		mov		digits+1, #0
		mov		digits+2, #0
		mov		digits+3, #0
		mov		digits+4, #0
		ret

;--------------------------------------------------------------------------------------------------------
; A - integer value

setDigitsInt:
		mov		DPTR, #DigitsTable
	
		; Set segments from right	
		mov		R0, #digits+5
	setDigitsInt_loop:
		dec		R0
		mov		B, #10
		div		AB
		xch		A, B
		movc	A, @A+DPTR
		mov		@R0, A
		mov		A, B
		cjne	A, #0, setDigitsInt_next

		; Set empty segments
	setDigitsInt_loopZero:
		cjne	R0, #digits, setDigitsInt_setZero
		ret

	setDigitsInt_setZero:
		dec		R0
		mov		@R0, #0h
		jmp		setDigitsInt_loopZero

		; Jump to next segment
	setDigitsInt_next:
		cjne	R0, #digits, setDigitsInt_loop
		ret

;--------------------------------------------------------------------------------------------------------

DigitsTable:
		db		3Fh, 6h, 5Bh, 4Fh, 66h, 6Dh, 7Dh, 7h, 7Fh, 6Fh

;--------------------------------------------------------------------------------------------------------

showNextDigit:
		push	ACC
		push	B

		mov		A, P2
	
		; Zero segments
		mov		P2,#0FFh
		mov		P7,#0h
		clr		DIGITSAVE
		setb	DIGITSAVE
	
		; Move active segment
		cjne	A, #0FEh, showNextDigit_move
		mov		A, #0DFh
	showNextDigit_move:
		rr		A
		mov		P2, A
	
		; Get address of segment
	 	mov		A, timerCounter
		mov		B, #5
		div		AB
		mov		A,#digits
		add		A,B
	
		; Set segment
		mov		R0, A
		mov		A, @R0
		mov		P7, A

		; zapisz stan segmentu
		clr		DIGITSAVE
		setb	DIGITSAVE

		pop		B
		pop		ACC
		ret

END
